export declare class SetWithRoundRobin<T> extends Set<T> {
    _infiniteEntryIterator: Iterator<T | undefined>;
    constructor();
    nextValue(): T | undefined;
}
//# sourceMappingURL=SetWithRoundRobin.d.ts.map