import { Unit } from "../Unit";
import WebSocket from 'ws';
import { InitialContextData } from "../interfaces";
export declare function receiveResponse(this: Unit, cbid: number, err: any, ctx?: InitialContextData, ws?: WebSocket): void;
//# sourceMappingURL=receiveResponse.d.ts.map